<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesOperacionesCielo" FilePath="css/style-ResponsabilidadSocial.css" PathNameAlias="SkinPath" />



<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />

<div id="hed-top-green" class="bg-mineral-green d-flex">
    <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white" href="/en-us">
                English
            </a>
            <span>
            </span>
        </li>
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white active" href="/es-es">
                Español
            </a>
            <span>
            </span>
        </li>
    </ul>
    <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link active" href="#">
                <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-1.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-1.png"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-2.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-2.png"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-3.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-3.png"></span>
            </a>
        </li>
    </ul>
</div>
<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
    <div class="navigation-top bg-menu-trans">
        <div class="wrap d-flex  justify-content-between">
            <div id="logo-header" class="logo-header text-center position-relative">
                <dnn:LOGO runat="server" ID="dnnLOGO" />
                <div class="d-flex justify-content-center align-items-center line-logo">
                    <span class="d-flex"></span>
                </div>
            </div>
            <div class="d-flex box-menu">
                <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
            </div>
        </div>
    </div>
</header>
<body>
	<section>
	  	<div id="ContentPane" runat="server"></div>
	</section>
	<section class="nav">
		<div class="w-100">
			<div id="RunatNav" runat="server"></div>
		</div>
	</section>
    <div class="separador" style="height: 94px;"></div>
	<div class="content_AS">
        <div class="content_S">
	  		<div id="section0" runat="server"></div>
            <div class="separador" style="height: 80px; width: 100%;"></div>
        </div>
        <div class="content_A">
	  		<div id="section1" runat="server"></div>

            
            <div class="box_minero">
                <picture class="content-img img">
                    <source media="(min-width: 991px)" srcset="/Portals/0/Images/OperacionesEspeciales/boletin-img.jpg">
                    <img src="/Portals/0/Images/OperacionesEspeciales/boletin-img.jpg" >
                </picture>
                <div class="title">
                    <span>Conoce nuestros</span>
                    Boletines
                    <br>
                    de prensa
                </div>
                <a href="#" class="buttom">
                    Ver boletines
                    <svg style="width: 54px; height: 26px;">
                        <g>
                            <path d="M40.6,26.1c-0.1,0-0.2,0-0.3-0.1c-0.1-0.1-0.1-0.4,0-0.5L52.7,13L40.3,0.6c-0.1-0.1-0.1-0.4,0-0.5
                                c0.1-0.1,0.4-0.1,0.5,0L53.8,13L40.9,26C40.8,26,40.7,26.1,40.6,26.1z"/>
                            <path d="M53.3,13.4H0.4C0.2,13.4,0,13.2,0,13s0.2-0.4,0.4-0.4h52.9c0.2,0,0.4,0.2,0.4,0.4S53.5,13.4,53.3,13.4z"/>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="box_sus">
                <div class="arr">
                    <svg style="width: 54px; height: 55px;">
                    <g>
                        <path d="M39.2,52c-4.8,2.1-8.9,2.8-14.5,2.8C11.6,54.9,0,45.5,0,30C0,14,11.8,0,29.6,0c14,0,24,9.5,24,22.8
                            c0,11.6-6.5,18.8-15.1,18.8c-3.7,0-6.4-2-6.8-6.1h-0.2c-2.5,3.9-6,6.1-10.3,6.1c-5.1,0-8.9-3.9-8.9-10.3c0-9.7,7.2-18.4,18.6-18.4
                            c3.5,0,7.4,0.9,9.4,1.9L38,29.5c-0.7,4.7-0.2,6.8,2.1,6.9c3.4,0.2,7.7-4.2,7.7-13.3c0-10.3-6.7-18.2-18.9-18.2
                            c-12.2,0-22.7,9.4-22.7,24.5C6.1,42.6,14.5,50,26.3,50c4,0,8.4-0.9,11.6-2.4L39.2,52z M32.3,19.5c-0.6-0.2-1.5-0.3-2.5-0.3
                            c-5.2,0-9.4,5.1-9.4,11.2c0,3,1.3,4.9,3.9,4.9c3,0,6.1-3.7,6.7-8.3L32.3,19.5z"/>
                    </g>
                    </svg>
                </div>
                <div class="text">
                    <strong>Suscríbase aquí </strong>
                    para recibir información corporativa de interés en su correo electrónico.
                </div>
                <input type="email" name="email" class="email" placeholder="Inscríbete">
            </div>
        </div>
    </div>
</body>
<footer class="bg-mineral-green" data-aos="fade-down">
    <dnn:MENU ID="MenuFooter" MenuStyle="Menus/FooterMenu" runat="server" NodeSelector="*" />
</footer>
