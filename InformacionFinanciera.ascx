<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInternaInforFinanciera" FilePath="css/informacion-financiera.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />

<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" /> 
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<body class="informacion-financiera">
	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<div id="hed-top-green" class="bg-mineral-green d-flex">
        <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us">
                    English
                </a>
                <span>
                </span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">
                    Español
                </a>
                <span>
                </span>
            </li>
        </ul>
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                    <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-1.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-1.png"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-2.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-2.png"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-3.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-3.png"></span>
                </a>
            </li>
        </ul>
    </div>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
        </div>
    </header>    
	<section id="baner" class="section-baner">
	    <div class="container-fluid">
	        <div class="row min-height" data-aos="fade-left">
	            <div class="col col-12 col-lg-4 col-md-6 bg-fern content-title d-flex justify-content-end align-items-center px-sm-0">
	                <img src="<%=SkinPath %>images/adorno-baner.png" srcset="<%=SkinPath %>images/adorno-baner.svg" class="adorno">
	                <div id="caja" runat="server" class="bg-white caja position-relative" data-aos="fade-up"></div>
	            </div>
	            <!--.col-12-->
	            <div id="columnRightBackground" runat="server" class="col col-12 col-lg-8 col-md-6 bg-img px-sm-0"></div>
	            <div class="line-color"></div>
	        </div>
	    </div><!-- /.container-fluid -->
	</section><!--#baner-->
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-pumice">
                    <li class="breadcrumb-item" data-aos="fade-right">
                        <a href="#" class="librefranklin-thin color-nevada color-hover-nevada d-flex align-items-center">
                            <img src="<%=SkinPath %>images/home.png" srcset="<%=SkinPath %>images/home.svg" class="icon"> Inicio
                        </a>
                    </li>
                    <li class="breadcrumb-item librefranklin-thin color-nevada active" aria-current="page" data-aos="fade-right">Información Financiera</li>
                </ol>
            </nav>
        </div><!--.container-->
    </section><!--.navigation-->
    <section class="section-informacion" id="informacion">
        <div class="container px-sm-0">
            <div class="row m-0">
                <div class="col-12 col-sm-12 col-md-12 col-lg-9 column-content px-md-0">
                    <div class="row m-0 dividendos">
                        <div id="titleInformacion" class="col-12 px-0" runat="server"></div><!--.col-12-->
                        <div class="col-12 paragraph-content">
                        	<div id="informacionParrafo1" class="paragraph color-scorpion librefranklin-regular" runat="server"></div>
                        	<div id="informacionParrafo2" class="paragraph color-scorpion librefranklin-regular" runat="server"></div>
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.row-->
                    <div class="row m-0 participacion">
						<div id="participacionTitle" class="col-12 title px-0 d-flex" runat="server"></div><!--.title-->
                        <div id="participacionPane" class="col-12 px-0 m-negativo" runat="server" data-aos="fade-up-right"></div>
                            <!--.box-link-->
                        <div class="col-12 px-0 d-flex flex-column flex-sm-row">
                            <div class="card box-link w-50 rounded-0 ml-0 border-color-alto" data-aos="fade-up-left">
                                <div class="card-body text-center pb-0">
                                	<div id="cardSuperIntendenciaTitle" runat="server" class="" data-aos="fade-left"></div>
                                </div>
                                <div class="card-footer bg-transparent border-0 p-0">
                                	<div id="cardSuperIntendenciaButtom" runat="server" class="" data-aos="fade-left"></div>
                                </div><!--.card-footer-->
                            </div>
                            <!--.box-link-->
                            <div class="card box-link w-50 rounded-0 mr-0 border-color-alto" data-aos="fade-up-right">
                                <div class="card-body text-center pb-0">
                                	<div id="cardBolsaDeValoresTitle" runat="server" class="" data-aos="fade-right"></div>
                                </div>
                                <div class="card-footer bg-transparent border-0 p-0">
                                	<div id="cardBolsaDeValoresButtom" runat="server" class="" data-aos="fade-right"></div>
                                </div><!--.card-footer-->
                            </div>
                            <!--.box-link-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.participacion-->
                    <div class="row m-0 presentacion">
                        <div class="col-12 title px-0 d-flex">
                        	<div id="presentacionResultadoTitle" class="col-12 title px-0 d-flex" runat="server"></div>
                        </div>
                        <div class="col-12 px-0">
                        	<!--<div id="presentacionResultadoCollapse" runat="server"></div>-->
                            <div class="collapse-conatiner">
                                <a class="btn btn-link btn-collpase rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-aos="fade-left">
                                    <span class="icon-collapse icon-rotateZ"><img src="<%=SkinPath %>images/arrow-circle-top.png" width="27.068" height="27.07" data-aos="flip-left"></span><span class="text librefranklin-bold color-mineral-green">2018</span>
                                </a>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body border-color-alto rounded-0">
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno1" role="button" aria-expanded="false" aria-controls="collapseExampleInterno1">
                                                <span class="icon-collapseExampleInterno"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">III TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno1">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno2" role="button" aria-expanded="false" aria-controls="collapseExampleInterno2">
                                                <span class="icon-collapseExampleInterno"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">II TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno2">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno3" role="button" aria-expanded="false" aria-controls="collapseExampleInterno3">
                                                <span class="icon-collapseExampleInterno"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">I TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno3">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                    </div>
                                    <!--.card-->
                                </div>
                            </div>
                            <!--.collapse-conatiner-->
                            <div class="collapse-conatiner">
                                <a class="btn btn-collpase btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample" data-aos="fade-right">
                                    <span class="icon-collapse icon-rotateZ"><img src="<%=SkinPath %>images/arrow-circle-top.png" width="27.068" height="27.07" data-aos="flip-left"></span><span class="text librefranklin-bold color-mineral-green">2017</span>
                                </a>
                                <div class="collapse" id="collapseExample2">
                                    <div class="card card-body border-color-alto rounded-0">
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno1" role="button" aria-expanded="false" aria-controls="collapseExampleInterno1" data-aos="fade-left">
                                                <span class="icon-collapseExampleInterno" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">III TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno1">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros III trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno2" role="button" aria-expanded="false" aria-controls="collapseExampleInterno2" data-aos="fade-right">
                                                <span class="icon-collapseExampleInterno" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">II TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno2">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros II trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                        <div class="collapse-trimestres">
                                            <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#collapseExampleInterno3" role="button" aria-expanded="false" aria-controls="collapseExampleInterno3"  data-aos="fade-left">
                                                <span class="icon-collapseExampleInterno" data-aos="flip-left"><img src="<%=SkinPath %>images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                <span class="text librefranklin-bold color-scorpion">I TRIMESTRE</span>
                                            </a>
                                            <div class="collapse" id="collapseExampleInterno3">
                                                <div class="card card-body rounded-0 border-color-alto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Webcast presentación de resultados I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                            <a href="#" class="text-decoration-none d-flex">
                                                                <span class="icon-pdf" style="margin-right: 21px;">
                                                                    <img src="<%=SkinPath %>images/icon-pdf-2.png" srcset="<%=SkinPath %>images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                </span>
                                                                <p class="mb-0 color-scorpion librefranklin-regular">Resultados financieros I trimestre</p>
                                                                <span class="icon-download ml-auto">
                                                                    <img src="<%=SkinPath %>images/icon-download.png" srcset="<%=SkinPath %>images/icon-download.svg" alt="" width="16" alt="16">
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <!--.list-group-item-->
                                                    </ul>
                                                    <!--.list-group-->
                                                </div>
                                                <!--.card-->
                                            </div>
                                            <!--.collapse-->
                                        </div>
                                        <!--.collapse-trimestres-->
                                    </div>
                                    <!--.card-->
                                </div>
                            </div>
                            <!--.collapse-conatiner-->
                        </div>
                        <!--.title-->
                    </div>
                    <!--.presentacion-->
                    <div class="row m-0 consolidados">
                        <div class="col-12 title px-0 d-flex">
                            <div id="InformesFinancierosTitle" runat="server"></div>
                        </div>
                        <!--.title-->
                        <div id="InformesFinancierosPanel" class="col-12 px-0 d-flex m-negativo" runat="server" data-aos="fade-up-left"></div>
                    </div>
                    <!--.consolidados-->
                    <div class="row m-0 relacion">
                        <div class="col-12 px-0">
                            <div class="card bg-jungle-green border-0 rounded-0" data-aos="fade-up-left">
                                <div class="card-body">
                                    <div id="RelacionInversionistasTitle" runat="server"></div>
                                    <div id="RelacionInversionistasList" runat="server"></div>
                                </div><!--.card-body-->
                            </div><!--.card-->
                        </div>
                    </div>
                    <!--.relacion-->
                </div>
                <!--.column-content-->
                <div class="col-12 col-lg-3 aside pr-0">
                    <div class="row m-0 indicadores">
                        <div class="col-12 d-flex align-items-end pr-0 mb-3">
                            <div class="indicadores-ico bg-corn d-flex justify-content-center align-items-center" data-aos="flip-left">
                                <img src="<%=SkinPath %>images/indicadores-ico.png" srcset="<%=SkinPath %>images/indicadores-ico.svg" class="img-fluid" alt="Indicadores image">
                            </div>
                            <h3 class="h4 title mb-0 w-100 border-color-alto librefranklin-bold color-scorpion" data-aos="fade-left">Indicadores</h3>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-feijoa border-0 rounded-0" data-aos="fade-right">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in">ACCIÓN <span class="d-block librefranklin-bold">MINEROS</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">1.920,00 <span class="moneda">COP</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular color-mineral-green" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold color-mineral-green">04-03-2019 06:08 PM</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-fern border-0 rounded-0" data-aos="fade-left">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in">Valor <span class="d-block librefranklin-bold">de Oro</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">1.286,06 <span class="moneda">USD/Oz</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular color-mineral-green" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold color-mineral-green">04-03-2019 01:10 PM</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-jungle-green border-0 rounded-0" data-aos="fade-right">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in">Valor <span class="d-block librefranklin-bold">de la plata</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">15,22 <span class="moneda">USD/Oz</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular color-mineral-green" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold color-mineral-green">04-03-2019 08:00 P M</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0">
                            <div class="card bg-mineral-green border-0 rounded-0" data-aos="fade-left">
                                <span class="span-simbolo position-absolute">$</span>
                                <div class="card-header border-0 rounded-0 bg-transparent position-relative">
                                    <h3 class="h4 card-title text-white librefranklin-regular text-uppercase" data-aos="zoon-in"><span class="d-block librefranklin-bold">TRM</span></h3>
                                    <p class="card-text text-white librefranklin-bold" data-aos="zoon-in-up">0 <span class="moneda">COP</span></p>
                                </div>
                                <!--.card-header-->
                                <div class="card-body">
                                    <p class="card-text librefranklin-regular text-white" data-aos="zoon-in-left">Última marcación <span class="d-block librefranklin-bold text-white">04-03-2019 06:08 PM</span></p>
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div><!--.indicadores-->
                    <div class="row m-0 tarjetas">
                      <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre">
                        <div id="ResultadosFinancieros" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div><!--.card-->
                      </div><!--.col-12-->
                      <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                        <div id="BoletinesDePrensa" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div><!--.card-->
                      </div><!--.col-12-->
                    </div><!--.tarjetas-->
                    <div class="row m-0 sucribir">
                      <div class="col-12 col-sm-9 col-lg-12 m-auto pr-0">
                        <div class="card rounded-0 bg-jungle-green" data-aos="fade-up-right">
                          <div class="card-body rounded-0 text-center">
                            <div data-icon="a" class="icon" style="color: #fff;"></div>
                            <h3 class="h4 mb-0 text-center text-white librefranklin-bold" data-aos="fade-up-left">Suscríbase aquí</h3>
                            <p class="h4 mb-0 card-text text-white text-center librefranklin-regular" data-aos="fade-up-right">para recibir información corporativa de interés en su correo electrónico.</p>
                          <div class="card-footer border-0 rounded-0 bg-transparent">
                            <form class="form-suscriber">
                              <div class="form-group">
                                <label for="exampleInputEmail1" class="sr-only" data-aos="fade-right">Email address</label>
                                <input type="email" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Inscríbete" data-aos="fade-right">
                              </div>
                              <button type="submit" class="btn btn-link text-decoration-none librefranklin-bold text-white d-none">Submit</button>
                            </form>
                          </div><!--.card-footer-->
                          </div><!--.card-body-->
                        </div><!--.card-->
                      </div><!--.col-12-->
                    </div><!--.sucribir-->
                </div><!--.aside-->
            </div><!--.row-->
        </div><!--.container-->
    </section><!--.section-informacion-->	    	
    <div id="ContentPane" runat="server"></div>

</body>