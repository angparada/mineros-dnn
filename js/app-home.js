$( document ).ready(function() {
      
      var myChart = echarts.init(document.getElementById('box-graf-section-3'));
      var colors = ['#676767', '#000', '#675bba'];
      var option = {
        media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {       // write options accordingly
                    dataZoom: [
                        {
                            type: 'slider',
                            xAxisIndex: [0],
                            realtime: false,
                            filterMode: 'weakFilter',
                            start: 1,
                            end: 40,
                            bottom: 8,
                            height: 20,
                            handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                            handleSize: '120%',
                            showDetail: false
                        },
                    ],
                    legend: {
                        left: 'right',
                        itemwidth: 46,
                        itemHeight: 7
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['Ene’ 17', 'Feb’ 17', 'Mar’ 17', 'Abr’ 17', 'May’ 17', 'Jun’ 17', 'Jul’ 17', 'Ago’ 17', 'Sep’ 17', 'Oct’ 17', 'Nov’ 17', 'Dic’ 17'],
                        min: 'Ene’ 17',
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        name: 'Precio (COP $)',
                        nameLocation: 'middle',
                        nameRotate: 270,
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 16,
                            align: 'center',
                            verticalAlign:'middle',
                            lineHeight: 19.2,
                            padding: [0, 0, 15, 0]
                        },
                        position: 'right',
                        maxInterval: 1,
                        splitNumber: 1,
                        boundaryGap: false,
                        min: 9,
                        max: 13,
                        data: [9,10,11,12],
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '{value}k',
                            verticalAlign: 'bottom'
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    series: [{
                        name: ' ',
                        data: [10, 10.4, 10.1, 10.3 ,10.8 ,11 ,10.1, 10.01, 10.5, 10.8, 10.7, 10.6],
                        type: 'line',
                        smooth: true,
                        symbol: 'none',
                    }],
                    animationEasing: 'elasticOut',
                    color: '#daa900',
                    
                }
            },
            {
                query: {
                    maxWidth: 550
                },   // write rule here
                option: { 
                    dataZoom: [
                        {
                            type: 'slider',
                            xAxisIndex: [0],
                            realtime: false,
                            filterMode: 'weakFilter',
                            start: 1,
                            end: 40,
                            bottom: 8,
                            height: 20,
                            handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                            handleSize: '120%',
                            showDetail: false
                        },
                    ],
                    legend: {
                        left: 'right',
                        itemwidth: 46,
                        itemHeight: 7
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['Ene’ 17', 'Feb’ 17', 'Mar’ 17', 'Abr’ 17', 'May’ 17', 'Jun’ 17', 'Jul’ 17', 'Ago’ 17', 'Sep’ 17', 'Oct’ 17', 'Nov’ 17', 'Dic’ 17'],
                        min: 'Ene’ 17',
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        name: 'Precio (COP $)',
                        nameLocation: 'middle',
                        nameRotate: 270,
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 16,
                            align: 'center',
                            verticalAlign:'middle',
                            lineHeight: 19.2,
                            padding: [0, 0, 10, 0]
                        },
                        position: 'right',
                        maxInterval: 1,
                        splitNumber: 1,
                        boundaryGap: false,
                        min: 9,
                        max: 13,
                        data: [9,10,11,12],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '{value}k',
                            verticalAlign: 'bottom'
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    series: [{
                        name: ' ',
                        data: [10, 10.4, 10.1, 10.3 ,10.8 ,11 ,10.1, 10.01, 10.5, 10.8, 10.7, 10.6],
                        type: 'line',
                        smooth: true,
                        symbol: 'none',
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#daa900',
                    
                }
            },
        ],
        
    };
      myChart.setOption(option);

      var inverSocial = echarts.init(document.getElementById('inversion-social'));
      var colors = ['#676767', '#000', '#675bba'];
      var option = {
            media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign:'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0,675,1350,2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                       // bottom: 90
                    },
                    series: [{
                        name: ' ',                
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap:'10%',
                        data: [2025, 2600, 2300],
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {                
                query: {
                    maxWidth: 550
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign:'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0,675,1350,2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                       // bottom: 90
                    },
                    series: [{
                        name: ' ',                
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap:'10%',
                        data: [2025, 2600, 2300],
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {                
                query: {
                    maxWidth: 425
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign:'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0,675,1350,2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                       // bottom: 90
                    },
                    series: [{
                        name: ' ',                
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap:'10%',
                        data: [2025, 2600, 2300],
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }]            
        };
     inverSocial.setOption(option);


});