<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInternaBoletines" FilePath="css/InternaBoletines.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />

<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<body>
	<div id="ContentPane" runat="server"></div>
     <section id="baner" class="section-baner">
      <div class="container-fluid">
        <div class="row">
          <div class="col col-12 col-lg-4 bg-fern content-title d-flex justify-content-center align-items-center">
            <img src="<%=SkinPath %>images/adorno-baner.png" srcset="<%=SkinPath %>images/adorno-baner.svg" class="adorno">
            <h1 class="title text-white d-inline-block librefranklin-thin">
              Boletines
              <span class="d-block text-white subtitulo librefranklin-bold">
                Boletines de prensa
              </span>
            </h1>

          </div>
          <div class="col col-12 col-lg-8 bg-img" style="background-image: url('<%=SkinPath %>images/img-boletin.png');">
          </div>
          <div class="line-color">
            
          </div>
          <div class="col col-12 bg-pumice nav barragris">
            <div class="container d-flex align-items-center flex-wrap">
              <a href="#" class="nav-item librefranklin-thin color-nevada color-hover-nevada d-inline-flex align-items-center">
                <img src="<%=SkinPath %>images/home.png" srcset="<%=SkinPath %>images/home.svg" class="icon">
                Inicio
              </a>
              <a href="#" class="nav-item librefranklin-thin color-nevada color-hover-nevada d-inline-flex align-items-center">
                <!-- <img src="<%=SkinPath %>images/home.png" srcset="<%=SkinPath %>images/home.svg" class="icon"> -->
                Boletines de prensa
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="section-principal" class="section-principal">
        <div class="container">
          <div class="row">
              
            <div class="col-lg-9">
                <div class="bg2-imagen">
                    <img src="<%=SkinPath %>images/img-bol1.png" class="img-fluid">
                </div>
                <div class="d-flex flex-wrap">
                    <div class="fechapost bg-golden-grass"><h1 class="librefranklin-bold formatpost text-white text-center">Dic</h1><h1 class="librefranklin-regular formatpost text-white text-center">29</h1><h1 class="librefranklin-regular formatyear text-white text-center">2018</h1></div>
                    <div class="titulopost librefranklin-bold color-nevada">Lorem ipsum dolor sit amet, consectetuer  Lorem ipsum dolor sit amet, consectetuer</div>
                </div>
                <div class="">
                    <p class="textopost librefranklin-regular color-nevada">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate.</p>

                    <p class="textopost librefranklin-regular color-nevada"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem </p> 
                </div>
                <div class="row gallery m-0">
                    <div class="col-lg-6 text-center">
                       <img src="<%=SkinPath %>images/img-bol2.png" class="imagen1 img-fluid">
                    </div>
                    <div class="col-lg-6 text-center">
                       <img src="<%=SkinPath %>images/img-bol3.png" class="imagen2 img-fluid">
                    </div>
                </div>
                <div class="">
                    <p class="textopost librefranklin-regular color-nevada">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br>
                    Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br>
                    Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                </div>
            </div> 
            <div class="col-lg-3 aside">
                <div class="card">
                  <h5 class="card-header titulo1 bg-golden-grass .librefranklin-semibold formatlibre text-white">Resultados financieros</h5>
                  <div class="card-body titulo2 bg-jungle-green">
                    <p class="card-title librefranklin-semibold formatlibre2 text-white">Tercer trimestre</p>
                    <p class="card-text librefranklin-semibold formatlibre3 text-white">2018</p>
                    <img src="<%=SkinPath %>images/adorno-aside.png" srcset="<%=SkinPath %>images/adorno-aside.svg" class="adornopost">
                    <img src="<%=SkinPath %>images/gold.png" srcset="<%=SkinPath %>images/golden.svg" class="golden">
                  </div>
                </div>
                <div class="card">
                  <div class="card-header customcard2">
                   <p class="card-title librefranklin-regular formatlibre4 text-white">Conoce nuestros</p>
                   <h5 class=".librefranklin-semibold formatlibrec2 text-white">Boletines de prensa</h5>
                  </div>
                  <div class="card-body bg-mineral-green customcard2body">
                    <blockquote class="blockquote mb-0">
                      <a href="#" class="aboletin librefranklin-regular text-white">Ver boletines</a><img src="<%=SkinPath %>images/flecha.png" srcset="<%=SkinPath %>images/flecha.svg" class="flecha">
                    </blockquote>
                  </div>
                </div>
                <div class="asidepos bg-jungle-green">
                    <img src="<%=SkinPath %>images/arroba.png" srcset="<%=SkinPath %>images/arroba.svg" class="arroba" >
                    <h1 class="librefranklin-bold text-white text-center formattexta">Suscríbase aquí </h1>
                    <p class="librefranklin-regular text-white formattextsub text-center">para recibir información corporativa de interés en su correo electrónico</p>
                    <div action="" method="get" class="formslocat text-center">
                       <input id="uniform" type="text" placeholder="Inscríbete" class="inputsubs form-control">
                    </div>
                </div>
          </div>
         </div>
        </div>
    </section>
    </body>
