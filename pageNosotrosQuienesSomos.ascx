<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesNosotrosQuienes" FilePath="css/style-nosotros-quienes-1.css" PathNameAlias="SkinPath" />


<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsNosotrosQuienes" FilePath="js/app-Nosotros-Quienes-Somos.js" PathNameAlias="SkinPath" />


<div id="hed-top-green" class="bg-mineral-green d-flex">
    <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white" href="/en-us">
                English
            </a>
            <span>
            </span>
        </li>
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white active" href="/es-es">
                Español
            </a>
            <span>
            </span>
        </li>
    </ul>
    <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link active" href="#">
                <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-1.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-1.png"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-2.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-2.png"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-3.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-3.png"></span>
            </a>
        </li>
    </ul>
</div>
<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
    <div class="navigation-top bg-menu-trans">
        <div class="wrap d-flex  justify-content-between">
            <div id="logo-header" class="logo-header text-center position-relative">
                <dnn:LOGO runat="server" ID="dnnLOGO" />
                <div class="d-flex justify-content-center align-items-center line-logo">
                    <span class="d-flex"></span>
                </div>
            </div>
            <div class="d-flex box-menu">
                <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
            </div>
        </div>
    </div>
</header>
<body class="QuienesSomos">
	<section id="baner" class="section-baner">
		<div id="ContentPane" runat="server"></div>
  	</section>
  	 <section id="info" class="section-info">
		<div id="infoRunaServer" runat="server"></div>
  	 </section>
  	 <section id="presencia" class="section-presencia">
      	<div class="container">
			<div id="PresenciaRunaServer" runat="server"></div>
        	<div class="content-info-ban" data-aos="fade-down">
				<div id="Ban1RunaServer" runat="server"></div>
				<div id="Ban2RunaServer" runat="server"></div>
				<div id="Ban3RunaServer" runat="server"></div>
        	</div>
		</div>
  	 </section>
</body>
<footer class="bg-mineral-green" data-aos="fade-down">
    <dnn:MENU ID="MenuFooter" MenuStyle="Menus/FooterMenu" runat="server" NodeSelector="*" />
</footer>