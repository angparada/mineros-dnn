<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesHome" FilePath="css/style-home.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />

<section class="overflowX-hidden">
	<div id="hed-top-green" class="bg-mineral-green d-flex">
        <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us">
                    English
                </a>
                <span>
                </span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">
                    Español
                </a>
                <span>
                </span>
            </li>
        </ul>
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                    <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-1.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-1.png"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-2.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-2.png"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span class="box-img-loc"><img src="/Portals/0/skins/mineros/images/svg/ban-3.svg"  srcset="/Portals/0/skins/mineros/images/2x/ban-3.png"></span>
                </a>
            </li>
        </ul>
    </div>
	<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
        </div>
    </header>
    <main>
        <section id="section-1" class="bg-fern">
            <div class="container-fluid p-0">
            	<div id="ContentPane" runat="server"></div>
            </div>
        </section>
        <section id="section-2" class="">
            <div class="container">
                <div class="row">
                    <div id="cab-section-2" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn" data-aos="fade-up">
                    	<div id="ResultadosFinancierosIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server"></div>
                    	<div id="ResultadosFinancierosTitulo" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server"></div>
                    </div>
                    <div class="box-section-2 position-relative d-flex w-100" data-aos="fade-right">
                        <div class="box-img-section-2 overflow-hidden position-relative">
                            <div id="ResultadosFinancierosBackground" runat="server"></div>
                            <div class="box-text-section-2 position-absolute bg-duocolor-corn-green">
                                <div id="ResultadosFinancierosTitulo2" runat="server"></div>
                                <div id="ResultadosFinancierosBoton" runat="server"></div>
                            </div>
                        </div>
                        <div class="section-text-lateral-text d-flex flex-column" data-aos="fade-left">
                            <div class="box-text-icon-section-2 bg-corn text-white d-flex justify-content-between align-items-center" data-aos="fade-right">
                                <h3 class="tittle-section-2-icon librefranklin-semibold text-white">Información Relevante</h3>
                                <a class="box-arrow transitionCss" href="#">
                                    <img srcset="/Portals/0/skins/mineros/images/2x/right-row.png" src="/Portals/0/skins/mineros/images/SVG/right-row.svg">
                                </a>
                            </div>
                            <div class="box-text-list bg-alto position-relative">
                                <ul class="list h-100 d-flex justify-content-around flex-column">
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Representancion de Accionistas (Res.116 de febr.27/2002)</a></li>
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Proyecto Utilidad o Perdida a presentar a Asamblea</a></li>
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Proyecto Utilidad o Perdida a presentar a Asamblea</a></li>
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Citación a Asamblea Ordinaria</a></li>
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Situaciones financieras del emisor</a></li>
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Códigos de Buen Gobierno</a></li>
                                    <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="#" class="text-decoration-none color-outer-space librefranklin-regular text-link-post">Cambio en la composición accionaria del emisor</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-3">
            <div class="container">
                <div class="row">
                    <div class="col-img-section-3 d-flex flex-column overflow-hidden" data-aos="fade-up-left">
                        <div class="box-img-col-section-3 overflow-hidden">
                            <div id="FundacionMinerosBackground" runat="server"></div>
                        </div>
                        <div class="box-tittle-1-col-section-3 d-flex align-items-end">
                            <span class="sub-box-tittle-1-section-3 bg-mineral-green d-flex align-items-center"  data-aos="flip-up">
                                <div id="FundacionMinerosTitulo1" runat="server"></div>
                            </span>
                            <span class="sub-box-line-bottom bg-corn col">

                            </span>
                        </div>
                        <div class="box-tittle-2-col-section-3 d-flex bg-fern justify-content-between align-items-center">
                            <div id="FundacionMinerosTitulo2" runat="server"></div>
                            <div id="FundacionMinerosBoton" runat="server"></div>
                        </div>
                    </div>
                    <div class="col-graf-section-3 d-flex flex-column overflow-hidden col" data-aos="fade-up-right">
                        <div id="cab-section-3" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn">
                            <div id="CashCostIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server">
                            </div>
                            <div id="CashCostTitle" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server">
                            </div>
                        </div>
                        <div id="box-graf-section-3" class="d-block border-color-jungle-green"  data-aos="flip-up">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-4" class="bg-duocolor-green-white">
            <div class="container">
                <div class="row">
                    <div id="" class="col-graf-section-4 d-flex flex-column overflow-hidden" data-aos="fade-up">
                        <div id="cab-section-4" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn">
                            <div id="InversionSocialIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server"></div>
                            <div id="InversionSocialTitle" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server"></div>
                        </div>
                        <div id="box-graf-section-4" class="d-block border-color-jungle-green bg-white" data-aos="flip-left">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item col p-0 border-0 rounded-0">
                                        <a class="nav-link rounded-0 border-0 librefranklin-semibold text-center active" id="inversion-social-tab" data-toggle="tab" href="#inversion-social" role="tab" aria-controls="inversion-social" aria-selected="true"  onclick="noChargeGraf()">Inversión Social</a>
                                    </li>
                                    <li class="nav-item col p-0 border-0 rounded-0">
                                        <a class="nav-link rounded-0 border-0 librefranklin-semibold text-center" id="action-social-tab" data-toggle="tab" href="#action-social" role="tab" aria-controls="action-social" aria-selected="false" onclick="chargeGraf()">Acción Social - Donaciones</a>
                                    </li>
                                </ul>
                                <div class="tab-content w-100 position-relative" id="myTabContent">
                                    <div class="tab-pane fade w-100 h-100 show active" id="inversion-social" role="tabpanel" aria-labelledby="inversion-social-tab"></div>
                                    <div class="tab-pane fade w-100 h-100" id="action-social" role="tabpanel" aria-labelledby="action-social-tab"></div>
                                </div>
                        </div>
                    </div>                    
                    <div class="col-box-section-4 d-flex flex-column overflow-hidden col" data-aos="fade-down">
                        <div id="box-sections" class="d-flex flex-wrap justify-content-between">
                                <div id="val-plata" class="rounded-0 simbol-dolar border-0 card border-success mb-3 target-med bg-corn position-relative overflow-hidden" style="max-width: 18rem;"  data-aos="flip-left">
                                    <div class="card-header bg-transparent border-success">
                                        <h4 class="text-left">
                                            <span class="title-target-sub text-uppercase text-target-card librefranklin-thin text-white d-block">VALOR</span>
                                            <span class="title-target-sub text-uppercase text-target-card librefranklin-semibold text-white d-block">DE LA PLATA</span>
                                        </h4>
                                    </div>
                                    <div class="card-body text-success border-bottom">
                                        <span class="val-numerico librefranklin-semibold text-white">15,22</span>
                                        <span class="val-text librefranklin-semibold text-white">USD/Oz</span>
                                    </div>
                                    <div class="card-footer bg-transparent border-success">
                                        <span class="text-footer-tittle-mark librefranklin-regular color-mineral-green d-block">Última marcación</span>
                                        <span class="text-footer-fecha-mark librefranklin-bold color-mineral-green d-block">04-03-2019 06:08 A M</span>
                                    </div>
                                </div>
                                <div id="val-oro" class="rounded-0 simbol-dolar card border-success mb-3 target-med bg-fern position-relative overflow-hidden" style="max-width: 18rem;"  data-aos="flip-left">
                                        <div class="card-header bg-transparent border-success">
                                            <h4 class="text-left">
                                                <span class="title-target-sub text-uppercase text-target-card librefranklin-thin text-white d-block">VALOR</span>
                                                <span class="title-target-sub text-uppercase text-target-card librefranklin-semibold text-white d-block">DEL ORO</span>
                                            </h4>
                                        </div>
                                        <div class="card-body text-success border-bottom">
                                            <span class="val-numerico librefranklin-semibold text-white">1.286,06</span>
                                            <span class="val-text librefranklin-semibold text-white">USD/Oz</span>
                                        </div>
                                        <div class="card-footer bg-transparent border-success">
                                            <span class="text-footer-tittle-mark librefranklin-regular color-mineral-green d-block">Última marcación</span>
                                            <span class="text-footer-fecha-mark librefranklin-bold color-mineral-green d-block">04-03-2019 06:08 A M</span>
                                        </div>
                                </div>
                                <div id="val-mineros" class="rounded-0 simbol-dolar-e card border-success mb-3 w-100 mw-100 espcial-card bg-jungle-green overflow-hidden" style="max-width: 18rem;"  data-aos="flip-left">
                                        <div class="card-header bg-transparent border-success p-0 border-0">
                                            <h4 class="text-left">
                                                <span class="title-target-sub text-uppercase text-target-card librefranklin-thin text-white d-block">ACCIÓN MINEROS</span>
                                            </h4>
                                        </div>
                                        <div class="card-body text-success border-bottom">
                                            <span class="val-numerico librefranklin-semibold text-white">1.920,00</span>
                                            <span class="val-text librefranklin-semibold text-white">COP</span>
                                        </div>
                                        <div class="card-footer bg-transparent border-success">
                                            <span class="text-footer-tittle-mark librefranklin-regular color-mineral-green d-block">Última marcación</span>
                                            <span class="text-footer-fecha-mark librefranklin-bold color-mineral-green d-block">04-03-2019 06:08 A M</span>
                                        </div>
                                </div>
                        </div>
                    </div>
                         
                </div>
            </div>
        </section>
        <section id="section-5" class="bg-alto">
            <div class="container">
                <div class="row justify-content-between">
                    <div id="col-img-section-5" class="d-flex flex-column"  data-aos="fade-right">
                        <div id="cab-section-5" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn"  data-aos="flip-left">
                            <div id="BoletinesDePrensaIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server"></div>
                            <div id="BoletinesDePrensaTitle" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server"></div>
                        </div>
                        <div class="slider-img-section-5 bg-white"  data-aos="flip-left">
                            <div id="carouselCards" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="carousel-item active" data-interval="10000">                                    
                                        <div class="card border-0">
                                            <div class="box-img-section-5 position-relative d-flex align-items-center">
                                                <div class="box-img-sub h-100 overflow-hidden d-flex align-items-center">
                                                    <img src="/Portals/0/skins/mineros/images/2x/section-5.jpg" class="card-img-top d-block" alt="...">
                                                </div>
                                                <div class="text-date bg-corn position-absolute d-flex justify-content-center flex-column align-items-center">
                                                    <span class="mes-section-5 d-block text-white librefranklin-semibold">Dic</span>
                                                    <span class="dia-section-5 d-block text-white librefranklin-regular">29</span>
                                                    <span class="ano-section-5 d-block text-white librefranklin-regular">2018</span>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselCards" role="button" data-slide="prev">
                                                  <span class="box-row-slider-section-5 bg-fern bg-hover-corn transitionCss" aria-hidden="true">
                                                      <img src="/Portals/0/skins/mineros/images/icons/2x/left.svg" srcset="/Portals/0/skins/mineros/images/icons/2x/left.png" alt="prev">
                                                  </span>
                                                  <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselCards" role="button" data-slide="next">
                                                  <span class="box-row-slider-section-5 bg-fern bg-hover-corn transitionCss" aria-hidden="true">
                                                        <img src="/Portals/0/skins/mineros/images/icons/2x/right.svg" srcset="/Portals/0/skins/mineros/images/icons/2x/right.png" alt="prev">
                                                  </span>
                                                  <span class="sr-only">Next</span>
                                                </a>
                                            </div>                                            
                                            <div class="card-body border-0 bg-white ">
                                                <h5 class="card-title librefranklin-semibold color-corduroy">Lorem ipsum dolor sit amet, consectetuer</h5>
                                                <p class="card-text librefranklin-regular color-corduroy">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
                                            </div>
                                            <div class="card-footer border-0 bg-white ">
                                                <a href="#" class="btn librefranklin-thin text-white bg-fern rounded-0 bg-hover-corn transitionCss">Leer más</a>
                                            </div>
                                      </div>
                                  </div>
                                  <div class="carousel-item" data-interval="10000">                                    
                                        <div class="card border-0">
                                            <div class="box-img-section-5 position-relative d-flex align-items-center">
                                                <div class="box-img-sub h-100 overflow-hidden d-flex align-items-center">
                                                    <img src="/Portals/0/skins/mineros/images/2x/section-5.jpg" class="card-img-top d-block" alt="...">
                                                </div>
                                                <div class="text-date bg-corn position-absolute d-flex justify-content-center flex-column align-items-center">
                                                    <span class="mes-section-5 d-block text-white librefranklin-semibold">Dic</span>
                                                    <span class="dia-section-5 d-block text-white librefranklin-regular">28</span>
                                                    <span class="ano-section-5 d-block text-white librefranklin-regular">2018</span>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselCards" role="button" data-slide="prev">
                                                  <span class="box-row-slider-section-5 bg-fern bg-hover-corn transitionCss" aria-hidden="true">
                                                      <img src="/Portals/0/skins/mineros/images/icons/2x/left.svg" srcset="/Portals/0/skins/mineros/images/icons/2x/left.png" alt="prev">
                                                  </span>
                                                  <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselCards" role="button" data-slide="next">
                                                  <span class="box-row-slider-section-5 bg-fern bg-hover-corn transitionCss" aria-hidden="true">
                                                        <img src="/Portals/0/skins/mineros/images/icons/2x/right.svg" srcset="/Portals/0/skins/mineros/images/icons/2x/right.png" alt="prev">
                                                  </span>
                                                  <span class="sr-only">Next</span>
                                                </a>
                                            </div>                                            
                                            <div class="card-body border-0 bg-white ">
                                                <h5 class="card-title librefranklin-semibold color-corduroy">Lorem ipsum dolor sit amet, consectetuer</h5>
                                                <p class="card-text librefranklin-regular color-corduroy">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
                                            </div>
                                            <div class="card-footer border-0 bg-white ">
                                                <a href="#" class="btn librefranklin-thin text-white bg-fern rounded-0 bg-hover-corn transitionCss">Leer más</a>
                                            </div>
                                      </div>
                                  </div>
                                  <div class="carousel-item" data-interval="10000">                                    
                                        <div class="card border-0">
                                            <div class="box-img-section-5 position-relative d-flex align-items-center">
                                                <div class="box-img-sub h-100 overflow-hidden d-flex align-items-center">
                                                    <img src="/Portals/0/skins/mineros/images/2x/section-5.jpg" class="card-img-top d-block" alt="...">
                                                </div>
                                                <div class="text-date bg-corn position-absolute d-flex justify-content-center flex-column align-items-center">
                                                    <span class="mes-section-5 d-block text-white librefranklin-semibold">Dic</span>
                                                    <span class="dia-section-5 d-block text-white librefranklin-regular">16</span>
                                                    <span class="ano-section-5 d-block text-white librefranklin-regular">2018</span>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselCards" role="button" data-slide="prev">
                                                  <span class="box-row-slider-section-5 bg-fern bg-hover-corn transitionCss" aria-hidden="true">
                                                      <img src="/Portals/0/skins/mineros/images/icons/2x/left.svg" srcset="/Portals/0/skins/mineros/images/icons/2x/left.png" alt="prev">
                                                  </span>
                                                  <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselCards" role="button" data-slide="next">
                                                  <span class="box-row-slider-section-5 bg-fern bg-hover-corn transitionCss" aria-hidden="true">
                                                        <img src="/Portals/0/skins/mineros/images/icons/2x/right.svg" srcset="/Portals/0/skins/mineros/images/icons/2x/right.png" alt="prev">
                                                  </span>
                                                  <span class="sr-only">Next</span>
                                                </a>
                                            </div>                                            
                                            <div class="card-body border-0 bg-white ">
                                                <h5 class="card-title librefranklin-semibold color-corduroy">Lorem ipsum dolor sit amet, consectetuer</h5>
                                                <p class="card-text librefranklin-regular color-corduroy">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
                                            </div>
                                            <div class="card-footer border-0 bg-white ">
                                                <a href="#" class="btn librefranklin-thin text-white bg-fern rounded-0 bg-hover-corn transitionCss">Leer más</a>
                                            </div>
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="col-enlaces-section-5" class="d-flex flex-column" data-aos="fade-left">
                        <div id="EnlacesDeInteresTitle" class="cab-enlace bg-duo-horizonta-green-trans"  data-aos="flip-right" runat="server"></div>
                        <div class="box-items">
                                <ul class="list-group round-0">
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Memorias de Sostenibilidad</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Línea ética</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Bienestar Ambiental</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Memorias de Sostenibilidad</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Línea ética</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Bienestar Ambiental</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item"  data-aos="flip-right">
                                        <div class="media">
                                            <div class="box-img-items-section-5">
                                                <img class="w-100" src="/Portals/0/skins/mineros/images/2x/item.svg" srcset="/Portals/0/skins/mineros/images/2x/item.png">
                                            </div>
                                            <div class="media-body">
                                                <h5 class="mt-0 librefranklin-regular color-corduroy m-0"><a href="#" class="text-decoration-none link-enlaces-interes  librefranklin-regular color-corduroy">Línea ética</a></h5>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-6">
            <div class="container">
                <div class="row justify-content-center">
                    <div id="FilialesGrupoMinerosTitle" class="section-6-tittle-box d-block w-100 text-center"  data-aos="fade-up" runat="server"></div>
                    <div class="section-6-content-box d-flex flex-wrap w-100 justify-content-between"  data-aos="fade-up">
                        <div class="col-img-gall"  data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG1" class="box-img" runat="server"></div>
                        </div>
                        <div class="col-img-gall"  data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG2" class="box-img" runat=server></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG3" class="box-img" runat="server"></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG4" class="box-img" runat="server"></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG5" class="box-img" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <dnn:MENU ID="MenuFooter" MenuStyle="Menus/FooterMenu" runat="server" NodeSelector="*" />
    </footer>
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAppHomee" FilePath="js/app-home.js" PathNameAlias="SkinPath" />
<script>
        function chargeGraf(){
            var pest2 = document.getElementById('action-social');
            pest2.classList.add('d-block');
            pest2.style.width = '100%';
            pest2.style.height = '100%';
            var donacion = echarts.init(document.getElementById('action-social'));
            var colors = ['#676767', '#000', '#675bba'];
            var option = {
                  media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign:'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0,675,1350,2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                       // bottom: 90
                    },
                    series: [{
                        name: ' ',                
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap:'10%',
                        data: [2025, 2600, 2300],
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {                
                query: {
                    maxWidth: 550
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign:'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0,675,1350,2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                       // bottom: 90
                    },
                    series: [{
                        name: ' ',                
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap:'10%',
                        data: [2025, 2600, 2300],
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {                
                query: {
                    maxWidth: 425
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle:{
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign:'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0,675,1350,2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel:{
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine:{
                            interval: 1,
                            lineStyle:{
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                       // bottom: 90
                    },
                    series: [{
                        name: ' ',                
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap:'10%',
                        data: [2025, 2600, 2300],
                        
                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }]  
            };
            donacion.setOption(option);
        }
        function noChargeGraf(){
            var pest2 = document.getElementById('action-social');
            pest2.classList.remove('d-block');
        }
    </script>
</section>